//
//  Networking.swift
//  WeatherApp
//
//  Created by Darko Jovanovski on 5/18/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

class Networking: NSObject {
    
    func getWeatherDataFor(latitude: Double, longitude: Double, completion: @escaping (Result<FinalWeather>) -> Void) {

        let apiKey = "a8466e52e65ca07341a94857c0210ff8"
        let endPoint = "http://api.openweathermap.org/data/2.5/weather?lat=\(latitude)&lon=\(longitude)&APPID=\(apiKey)&units=metric"
        
        guard let url = URL(string: endPoint) else { return completion(.Error("Invalid URL, we can't update your feed")) }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else { return completion(.Error(error!.localizedDescription)) }
            guard let data = data else { return completion(.Error(error?.localizedDescription ?? "ERROR"))
            }
            do {
                let myStruct = try JSONDecoder().decode(FinalWeather.self, from: data) // do your decoding here
                completion(.Success(myStruct))
            } catch let error {
                return completion(.Error(error.localizedDescription))
            }
            }.resume()
    }
}

enum Result<T> {
    case Success(T)
    case Error(String)
}
