//
//  Helpers.swift
//  WeatherApp
//
//  Created by Darko Jovanovski on 5/18/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit


struct CoreDataModel {
    static let entityName = "Weather"
    static let name = "name"
    static let country = "country"
    static let requestTime = "requestTime"
    static let temp = "temp"
    static let tempMin = "tempMin"
    static let tempMax = "tempMax"
    static let sunrise = "sunrise"
    static let sunset = "sunset"
}

struct SegueIDs {
    static let weatherSegue = "weatherSegue"
    static let showFetchedController = "showFetchedController"
    static let showWeatherFromFetch = "showWeatherFromFetch"

}

func showAlertWith(title: String, buttonTitle: String, message: String, style: UIAlertControllerStyle = .alert, viewController: UIViewController) {
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
    let action = UIAlertAction(title: buttonTitle, style: .default) { (action) in
        viewController.dismiss(animated: true, completion: nil)
    }
    alertController.addAction(action)
    viewController.present(alertController, animated: true, completion: nil)
}

