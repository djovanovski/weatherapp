//
//  NoInternetViewController.swift
//  WeatherApp
//
//  Created by Darko Jovanovski on 5/22/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit
import CoreData

class NoInternetViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var selectedWeather: Weather?
    private let managedContext = CoreDataManager.sharedInstance.persistentContainer.viewContext
    
    lazy var fetchedResultController: NSFetchedResultsController<NSFetchRequestResult> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataModel.entityName)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: CoreDataModel.requestTime, ascending: false)]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedContext, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()
    
    
    //MARK: - ViewController overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: FetchedResultTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: FetchedResultTableViewCell.reuseIdentifier)
        do {
            try self.fetchedResultController.performFetch()
            //TODO: setup empty view if thee is time left
        } catch let error  {
            print("ERROR FETCHING: \(error)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: UITableViewDelegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fetchedResultController.fetchedObjects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FetchedResultTableViewCell.reuseIdentifier, for: indexPath) as! FetchedResultTableViewCell
        
        if let weather = self.fetchedResultController.object(at: indexPath) as? Weather {
            cell.populateWith(weather: weather)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let weather = self.fetchedResultController.object(at: indexPath) as? Weather {
            self.selectedWeather = weather
            self.performSegue(withIdentifier: SegueIDs.showWeatherFromFetch, sender: self)
        }
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIDs.showWeatherFromFetch {
            if let destinationVC = segue.destination as? WeatherViewController, let selectedWeather = self.selectedWeather {
                destinationVC.weather = selectedWeather
            }
        }
    }
    
    
}
