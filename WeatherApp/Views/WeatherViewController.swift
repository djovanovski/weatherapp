//
//  WeatherViewController.swift
//  WeatherApp
//
//  Created by Darko Jovanovski on 5/18/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var tempMinLabel: UILabel!
    @IBOutlet weak var tempMaxLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    
    var weather: Weather?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupUI() {
        let title = "\(weather?.name ?? ""), \(weather?.country ?? "")"
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        
        self.title = title
        self.tempLabel.text = "Temperature: \(weather?.temp?.description ?? "")"
        self.tempMinLabel.text = "Minimum temperature: \(weather?.tempMin?.description ?? "")"
        self.tempMaxLabel.text = "Maximum temperature: \(weather?.tempMax?.description ?? "")"
        self.sunriseLabel.text = "Sunrise time: \(formatter.string(from: weather?.sunrise ?? Date()))"
        self.sunsetLabel.text = "Sunset time: \(formatter.string(from: weather?.sunset ?? Date()))"
        
    }
}
