//
//  MapViewController.swift
//  WeatherApp
//
//  Created by Darko Jovanovski on 5/18/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import CoreData

private let NO_INTERNET = "Looks like your internet is off, please refresh it or redirect to your offline saved weather locations."
private let REFRESH = "Refresh"
private let REDIRECT = "Redirect"
private let TITLE = "No internet connection"

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var locationManager = CLLocationManager()
    private var currentLocation: CLLocation?
    private var doubleTapGR = UITapGestureRecognizer()
    private var shouldMakeAPIcall = false
    private var selectedWeather: Weather?
    private let managedContext = CoreDataManager.sharedInstance.persistentContainer.viewContext

    //MARK: - ViewController ovverides
    override func viewWillAppear(_ animated: Bool) {
        self.updateUserInterface()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Private functions
    func updateUserInterface() {
        guard let status = Network.reachability?.status else { return }
        switch status {
        case .unreachable:
            self.showNoInternetAlert(title: TITLE, message: NO_INTERNET)
        case .wifi, .wwan:
            self.setupDoubleTapGr()
            self.setupLocationManager()
        }
    }
    
    private func setupLocationManager() {
        self.locationManager.delegate = self
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    private func setupDoubleTapGr() {
        self.doubleTapGR = UITapGestureRecognizer(target: self, action: #selector(MapViewController.handleTap(_:)))
        self.doubleTapGR.delegate = self
        self.doubleTapGR.numberOfTapsRequired = 2
        self.doubleTapGR.cancelsTouchesInView = false
        self.mapView.addGestureRecognizer(doubleTapGR)
    }
    
    @objc private func handleTap(_ gesture: UITapGestureRecognizer){
        self.shouldMakeAPIcall = true
    }
    

    
    private func parseData(data: FinalWeather) {

        managedContext.mergePolicy = NSOverwriteMergePolicy
        if let entity = NSEntityDescription.entity(forEntityName: CoreDataModel.entityName, in: managedContext) {
            
            let weatherCoreData = NSManagedObject(entity: entity, insertInto: managedContext)
            weatherCoreData.setValue(data.name, forKeyPath: CoreDataModel.name)
            weatherCoreData.setValue(data.sys.sunrise, forKeyPath: CoreDataModel.sunrise)
            weatherCoreData.setValue(data.sys.sunset, forKeyPath: CoreDataModel.sunset)
            weatherCoreData.setValue(Date(), forKeyPath: CoreDataModel.requestTime)
            weatherCoreData.setValue(data.main.temp, forKeyPath: CoreDataModel.temp)
            weatherCoreData.setValue(data.main.temp_max, forKeyPath: CoreDataModel.tempMax)
            weatherCoreData.setValue(data.main.temp_min, forKeyPath: CoreDataModel.tempMin)
            weatherCoreData.setValue(data.sys.country, forKeyPath: CoreDataModel.country)

            self.selectedWeather = weatherCoreData as? Weather
            
            do {
                try managedContext.save()
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.performSegue(withIdentifier: SegueIDs.weatherSegue, sender: self)
                }

            } catch _ as NSError {
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    showAlertWith(title: "Error", buttonTitle: "OK", message: "Error save to core data.", style: .alert, viewController: self)
                }
            }
        }
    }
    
    //MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        
        if currentLocation == nil {
            if let userLocation = locations.last {
                let viewRegion = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 2000, 2000)
                mapView.setRegion(viewRegion, animated: false)
                let myLocationButton = MKUserTrackingBarButtonItem(mapView: mapView)
                self.navigationItem.rightBarButtonItem = myLocationButton
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            self.showActionAlert()
        }
    }
    
    //MARK: - MKMapViewDelegate
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if shouldMakeAPIcall {
            self.activityIndicator.startAnimating()
            Networking().getWeatherDataFor(latitude: mapView.region.center.latitude, longitude: mapView.region.center.longitude) {[weak self] result in
                switch result {
                case .Success(let data):
                    self?.parseData(data: data)
                case .Error(let error):
                    DispatchQueue.main.sync {
                        self?.activityIndicator.stopAnimating()
                        showAlertWith(title: "Error", buttonTitle: "OK", message: error, style: .alert, viewController: self ?? UIViewController())
                    }
                }
            }
            shouldMakeAPIcall = false
        }
    }
    
    //MARK: - UIGestureRecognizerDelegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIDs.weatherSegue {
            if let destinationVC = segue.destination as? WeatherViewController, let selectedWeather = self.selectedWeather {
                destinationVC.weather = selectedWeather
            }
        }
    }
    
    //MARK: - Notificaiton
    @objc func statusManager(_ notification: NSNotification) {
        updateUserInterface()
    }
    
    //MARK: - Alert functions
    //TODO: - display it if you have any time to focus the camera if user location is disbled
    private func showActionAlert() {
        let alertController = UIAlertController(title: "Choose your region", message:nil, preferredStyle: .actionSheet)
        
        let europeAction = UIAlertAction(title: "Europe" , style: .default, handler: { alert in
        })
        let usAction = UIAlertAction(title: "United States" , style: .default, handler: { alert in
        })
        let cancel = UIAlertAction(title:"Cancel" , style: .cancel, handler: { alert in
        })
        
        alertController.addAction(europeAction)
        alertController.addAction(usAction)
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func showNoInternetAlert(title: String, message: String, style: UIAlertControllerStyle = .alert) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        let refreshAction = UIAlertAction(title: REFRESH, style: .default, handler: { alert in
            self.updateUserInterface()
        })
        let redirectAction = UIAlertAction(title: REDIRECT, style: .cancel, handler: { alert in
            self.performSegue(withIdentifier: SegueIDs.showFetchedController, sender: self)
        })

        alertController.addAction(refreshAction)
        alertController.addAction(redirectAction)

        self.present(alertController, animated: true, completion: nil)
    }
}
