//
//  FetchedResultTableViewCell.swift
//  WeatherApp
//
//  Created by Darko Jovanovski on 5/22/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

class FetchedResultTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    static var reuseIdentifier: String {
        return "FetchedResultTableViewCell"
    }
    
    func populateWith(weather: Weather) {
        self.cityLabel.text = weather.name ?? ""
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        timeLabel.text = formatter.string(from: weather.requestTime ?? Date())
    }
    
}
