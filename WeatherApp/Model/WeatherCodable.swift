//
//  WeatherCodable.swift
//  WeatherApp
//
//  Created by Darko Jovanovski on 5/18/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import Foundation


struct Main: Decodable {
    let humidity: Double
    let pressure: Double
    let temp: Double
    let temp_max: Double
    let temp_min: Double
}

struct Sys: Decodable {
    let message: Double
    let country: String
    let sunrise: Date
    let sunset: Date
}

struct FinalWeather: Decodable {
    let main: Main
    let name: String
    let sys: Sys
}


