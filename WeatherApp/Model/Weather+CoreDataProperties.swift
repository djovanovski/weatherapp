//
//  Weather+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Darko Jovanovski on 5/18/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//
//

import Foundation
import CoreData


extension Weather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Weather> {
        return NSFetchRequest<Weather>(entityName: "Weather")
    }

    @NSManaged public var name: String?
    @NSManaged public var weatherDescription: String?
    @NSManaged public var temp: NSNumber?
    @NSManaged public var tempMin: NSNumber?
    @NSManaged public var tempMax: NSNumber?
    @NSManaged public var sunrise: Date?
    @NSManaged public var sunset: Date?
    @NSManaged public var requestTime: Date?
    @NSManaged public var country: String?


}
